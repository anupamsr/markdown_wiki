import sched
from threading import Thread
import time
import logging


class Scheduler:
    def __init__(self, wait_time: int):
        self.wait_time = wait_time
        self.logger = logging.getLogger(__name__)
        self.s = sched.scheduler(time.time, time.sleep)

    def schedule_thread(self, action, argument):
        thread = Thread(
            target=self.start_scheduler, args=((action, argument),), daemon=True
        )
        thread.start()

    def start_scheduler(self, fn: tuple):
        self.s.enter(
            delay=self.wait_time, priority=1, action=self.schedule, argument=(fn,)
        )
        self.s.run()

    def schedule(self, fn: tuple):
        fn[0](fn[1])
        self.s.enter(
            delay=self.wait_time, priority=1, action=self.schedule, argument=(fn,)
        )
