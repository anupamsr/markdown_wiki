import psycopg2

from item import Metadata


class DB:
    def __init__(
        self,
        password,
        server="localhost",
        port="5432",
        user="postgres",
        db_name="tsdb",
        table_name="entry_tdb",
    ) -> None:
        self.conn = psycopg2.connect(
            database=db_name, user=user, password=password, host=server, port=port
        )
        self.table_name = table_name

    def __del__(self):
        self.conn.close()

    def write(self, time: str, doc_id: str, metadata: Metadata):
        cur = self.conn.cursor()
        cur.execute(
            f"INSERT INTO {self.table_name}(time,id,metadata) VALUES(%s,%s,%s);",
            (time, doc_id, metadata),
        )
        self.conn.commit()
        cur.close()

    def write(self, doc_id: str, metadata: Metadata):
        cur = self.conn.cursor()
        cur.execute(
            f"INSERT INTO {self.table_name}(id,metadata) VALUES(NOW(),%s,%s);",
            (doc_id, metadata),
        )
        self.conn.commit()
        cur.close()

    def get_recent(self, num=5) -> dict[str, str]:
        cur = self.conn.cursor()
        cur.execute(
            f"SELECT * from (SELECT DISTINCT ON (id) time, id FROM entry_tdb ORDER BY id, time DESC LIMIT {num}) AS tmp ORDER BY time DESC;"
        )
        rows = cur.fetchall()
        data = dict()
        for row in rows:
            data[row[0]] = row[1]
        self.conn.commit()
        cur.close()
        return data
