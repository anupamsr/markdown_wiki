package dataLayer

import (
	"context"
	"fmt"
	"time"

	_ "github.com/go-kivik/couchdb/v3" // The CouchDB driver
	"github.com/go-kivik/kivik/v3"
)

type ctxKey struct{}

type DB struct {
	client *kivik.Client
	ctx    context.Context
	db     *kivik.DB
}

type Entry struct {
	ID       string    `json:"_id"`
	Rev      string    `json:"_rev,omitempty"`
	Title    string    `json:"title"`
	Email    string    `json:"email"`
	Name     string    `json:"name"`
	Phone    int       `json:"phone"`
	Text     string    `json:"text"`
	Created  time.Time `json:"created"`
	Modified time.Time `json:"modified,omitempty"`
}

func NewDB(user string, password string, host string, port uint, dbName string) *DB {
	serverUrl := fmt.Sprintf("http://%s:%s@%s:%d/", user, password, host, port)
	client, err := kivik.New("couch", serverUrl)
	if err != nil {
		panic(err)
	}
	ctx := context.WithValue(context.TODO(), ctxKey{}, serverUrl+dbName)
	if ok, _ := client.DBExists(ctx, dbName); !ok {
		panic(dbName + "database not found")
	}
	return &DB{client: client, ctx: ctx, db: client.DB(ctx, dbName)}
}

func (db *DB) ReadItem(docID string) (Entry, error) {
	var entry Entry
	err := db.db.Get(db.ctx, docID).ScanDoc(&entry)
	return entry, err
}
