"use strict";
const md = window.markdownit({
  html: false,
  xhtmlOut: false,
  linkify: true,
  typographer: true,
  breaks: true,
  langPrefix: "language-",
});
md.use(markdownItAnchor, {
  permalink: markdownItAnchor.permalink.headerLink(),
});
md.use(markdownItTocDoneRight);
md.use(require("markdown-it-imsize"));
md.use(require("markdown-it-external-links"), {
  externalClassName: "entry-ext",
  internalClassName: "entry-int",
  internalDomains: ["localhost:8000"],
}); // Using this as selectors fail for simple urls e.g. [this](this)

async function renderMd() {
  const render = new Promise((resolve, reject) => {
    const result = md.render(
      document.getElementById("result-html").innerHTML.trim()
    );
    document.getElementById("result-html").innerHTML = result;
  });
  const colorLinks = new Promise((resolve, reject) => {
    [...document.getElementsByClassName("entry-int")].filter((el) => {
      var http = new XMLHttpRequest();
      http.open("HEAD", el.getAttribute("href"));
      http.onreadystatechange = function () {
        if (this.readyState == this.DONE) {
          if (this.status != 200) {
            el.classList.add("exists-not");
          }
        }
      };
      http.send();
      return true;
    });
  });

  await render.then(colorLinks);
}

renderMd();
