package main

import (
	"fmt"
	"os"

	"codeberg.org/anupamsr/markdown_wiki/dataLayer"
	"codeberg.org/anupamsr/markdown_wiki/handlers"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/pborman/getopt/v2"
	"gopkg.in/ini.v1"
)

func main() {
	set := getopt.New()
	address := *set.StringLong("address", 'a', "0.0.0.0", "server address")
	var port uint
	set.FlagLong(&port, "port", 'p', "server port").Mandatory()
	set.Parse(os.Args)

	cfg, err := ini.Load("config.ini")
	if err != nil {
		panic(err)
	}

	user := cfg.Section("CouchDB").Key("user").String()
	password := cfg.Section("CouchDB").Key("password").String()
	host := cfg.Section("CouchDB").Key("host").String()
	dbPort, err := cfg.Section("CouchDB").Key("port").Uint()
	entryDBName := cfg.Section("CouchDB").Key("entry_db").String()
	// config_db_name := cfg.Section("CouchDB").Key("config_db").String()
	if err != nil {
		panic(err)
	}

	db := dataLayer.NewDB(user, password, host, dbPort, entryDBName)

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.HTTPErrorHandler = handlers.CustomHTTPErrorHandler
	e.Static("/static", "static")
	e.GET("/", handlers.GetHandler("root", db))
	// e.GET("/wiki/:title", handlers.TitleHeadHandler)
	e.GET("/wiki/:title", handlers.GetHandler("title", db))

	e.Logger.Fatal(e.Start(fmt.Sprintf("%s:%d", address, port)))
}
