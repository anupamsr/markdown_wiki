.PHONY: all clean install run

all: build

clean:
	@rm -fv markdown_wiki
	@rm -fv $GOPATH/bin/markdown_wiki
	@go clean

build: tidy format
	go build ./...

tidy:
	go mod tidy

format:
	go fmt ./...

install: build
	go install

run: install
	markdown_wiki -p 8000
