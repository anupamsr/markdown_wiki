from typing import TypedDict, Union
from pydantic import BaseModel, EmailStr
from datetime import datetime
import couchdb_client


class Item(BaseModel):
    title: str
    email: Union[EmailStr, None] = None
    name: str
    phone: Union[int, None] = None
    text: str

    def to_entry(self):
        return couchdb_client.Entry(
            title=self.title,
            email=self.email,
            name=self.name,
            phone=self.phone,
            text=self.text,
        )


class ItemStored(BaseModel):
    title: str
    email: Union[EmailStr, None] = None
    name: str
    phone: Union[int, None] = None
    text: str
    created: datetime
    modified: Union[datetime, None] = None


def get_item(entry: couchdb_client.Entry):
    return ItemStored(
        title=entry.title,
        email=entry.email,
        name=entry.name,
        phone=entry.phone,
        text=entry.text,
        created=entry.created,
        modified=entry.modified,
    )


class Metadata(TypedDict):
    """
    action: C for created, U for updated, D for deleted
    """

    action: str
    time: Union[datetime, None]
