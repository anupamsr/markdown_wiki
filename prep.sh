#!/bin/sh

for jsfile in preview.js show.js; do
    browserify $jsfile -o static/js/$jsfile
    temp_file=$(mktemp)
    uglifyjs --compress --mangle -- static/js/$jsfile > $temp_file
    mv $temp_file static/js/$jsfile
done
