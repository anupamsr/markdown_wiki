import couchdb_client
import logging
import configparser


class Config:
    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read("config.ini")
        logging.basicConfig(
            filename=self.config["SERVER"]["log_file"],
            encoding="utf-8",
            level=logging.getLevelName(self.config["SERVER"]["log_level"]),
        )
        self.logger = logging.getLogger(__name__)
        self.conf_db = couchdb_client.DB(
            password=self.config["CouchDB"]["password"],
            db_name=self.config["CouchDB"]["config_db"],
        )
        self.entry_db = couchdb_client.DB(
            password=self.config["CouchDB"]["password"],
            db_name=self.config["CouchDB"]["entry_db"],
        )
        self.max_recent = int(self.config["SERVER"]["max_recent"])

    def get_conf_db(self) -> couchdb_client.DB:
        return self.conf_db

    def get_entry_db(self) -> couchdb_client.DB:
        return self.entry_db

    def get_max_recent(self) -> int:
        return self.max_recent
