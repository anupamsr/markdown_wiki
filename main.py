import logging
from datetime import datetime

import uvicorn
from babel.dates import format_timedelta
from fastapi import Cookie, FastAPI, Form, HTTPException, Request, Response, status
from starlette.responses import HTMLResponse, RedirectResponse
from starlette.staticfiles import StaticFiles
from starlette.templating import Jinja2Templates
import couchdb_client
from item import Item, ItemStored, get_item
from scheduler import Scheduler
from config import Config


def format_timedelta_filter(value):
    if isinstance(value, datetime):
        return format_timedelta(datetime.now() - value)
    return format_timedelta(datetime.now() - datetime.fromisoformat(value[:-1]))


config = Config()
logger = logging.getLogger(__name__)
app = FastAPI()
templates = Jinja2Templates(directory="templates")
app.mount("/static", StaticFiles(directory="static"), name="static")
templates.env.filters["format_timedelta"] = format_timedelta_filter
entry_db = config.get_entry_db()
recent_conf = config.get_conf_db().read_recent_conf()


def add_recent(title: str):
    l = [i for i in recent_conf.recent_items if not (i["title"] == title)]
    recent_conf.recent_items = l[: config.get_max_recent()]
    recent_conf.recent_items.append({"title": title, "time": datetime.now()})


@app.on_event("startup")
async def startup_event():
    """
    This is used to run scheduled tasks.
    """
    scheduler = Scheduler(5)
    scheduler.schedule_thread(config.get_conf_db().write_recent_record, recent_conf)


@app.get("/", response_class=HTMLResponse)
async def read_root(request: Request) -> Response:
    recent = await get_recent_changes()
    return templates.TemplateResponse(
        "main.html", {"request": request, "recent": recent}
    )


@app.exception_handler(404)
async def custom_404_handler(request, __) -> Response:
    logger.debug(f"EXCEPTION 404")
    recent = await get_recent_changes()
    return templates.TemplateResponse(
        "404.html",
        {
            "request": request,
            "detail": "I think you should try one of the existing pages",
            "recent": recent,
        },
        status_code=404,
    )


@app.get("/wiki/{title}", response_class=HTMLResponse)
async def show(request: Request, title: str) -> Response:
    try:
        item = await read_item(title)
        recent = await get_recent_changes()
        return templates.TemplateResponse(
            "index.html", {"request": request, "item": item, "recent": recent}
        )
    except HTTPException as e:
        if e.status_code == status.HTTP_404_NOT_FOUND:
            logger.debug(f"{title} not found, redirecting to edit")
            return RedirectResponse(url=f"/wiki/{title}/edit")
        else:
            raise e


@app.get("/wiki/{title}/edit", response_class=HTMLResponse)
async def edit(
    request: Request,
    title: str,
    name: str = Cookie(default=""),
    email: str = Cookie(default=""),
    phone: str = Cookie(default=""),
) -> Response:
    try:
        item = await read_item(title)
        logger.debug(f"{title} already exists, populating with existing text")
        return templates.TemplateResponse(
            "editor.html",
            {
                "request": request,
                "title": title.strip(),
                "text": item.text,
                "name": name,
                "email": email,
                "phone": phone,
            },
        )
    except HTTPException as e:
        if e.status_code == status.HTTP_404_NOT_FOUND:
            return templates.TemplateResponse(
                "editor.html",
                {"request": request, "title": title.strip(), "text": None},
            )
        else:
            raise e


@app.post("/submit", response_class=HTMLResponse)
async def login(
    request: Request,
    response: Response,
    title: str = Form(),
    email: str = Form(None),
    name: str = Form(),
    phone: str = Form(0),
    source: str = Form(),
) -> Response:
    try:
        item = Item(title=title, email=email, name=name, phone=phone, text=source)
        item = await create_or_update(item, response)
        logger.info(f"Submit successful for {title}, redirecting...")
        response = RedirectResponse(url=f"/wiki/{item.title}")
        if name is not None and name.strip() != "":
            response.set_cookie(key="name", value=name)
        if email is not None and email.strip() != "":
            response.set_cookie(key="email", value=email)
        response.set_cookie(key="phone", value=phone)
        response.status_code = status.HTTP_302_FOUND
        return response
    except HTTPException as e:
        logger.exception(f"ERR error submitting {title}")
        recent = await get_recent_changes()
        return templates.TemplateResponse(
            "404.html",
            {"request": request, "title": title, "detail": e.detail, "recent": recent},
        )


@app.get("/items/recent", response_model=list)
async def get_recent_changes(num: int = 5) -> list[str]:
    """
    This can also work, but we maintain our own lru because we like better control and what CouchDB returns is not very
    useful.
        recent = await read_item("recent")
        return entry_db.changes()
    :return: List of recent titles
    """
    try:
        logger.info(f"RECENT {num}")
        return list(map(lambda x: x.title, recent_conf.recent_items))[::-1][:num]
    except Exception:
        logger.exception("ERR calling recent")
        return []


@app.head("/wiki/{title}")
async def read_item(title: str) -> bool:
    logger.info(f"HEAD {title}")
    if not entry_db.is_present(title):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="not found")
    return True


@app.get("/items/{title}", response_model=ItemStored)
async def read_item(title: str) -> ItemStored:
    try:
        logger.info(f"GET {title}")
        return get_item(entry_db.read_entry(title))
    except couchdb_client.DBIdEmptyError:
        logger.exception("ERR no title provided")
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="no title provided"
        )
    except couchdb_client.DBIdNotFoundError:
        logger.exception(f"ERR '{title}' not found")
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="not found")
    except Exception:
        logger.exception("ERR returning empty")
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="db error"
        )


@app.post("/items", response_model=ItemStored, status_code=status.HTTP_200_OK)
async def create_or_update(item: Item, response: Response) -> ItemStored:
    logger.info(f"POST {item.title}")
    if item.title is None:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="cannot update without title"
        )
    try:
        entry = entry_db.write_entry(item.to_entry())
        if entry.modified is None:
            response.status_code = status.HTTP_201_CREATED
        add_recent(entry.title)
        return get_item(entry)
    except Exception:
        logger.exception("ERR returning empty")
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="db error"
        )


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=False)
