package handlers

import (
	"net/http"

	"codeberg.org/anupamsr/markdown_wiki/dataLayer"
	"github.com/labstack/echo/v4"
	"github.com/noirbizarre/gonja"
)

// Pre-compiling the templates at application startup using the
// little Must()-helper function (Must() will panic if FromFile()
// or FromString() will return with an error - that's it).
// It's faster to pre-compile it anywhere at startup and only
// execute the template later.
var errorTemplate = gonja.Must(gonja.FromFile("templates/404.html"))

func CustomHTTPErrorHandler(err error, c echo.Context) {
	code := http.StatusInternalServerError
	if he, ok := err.(*echo.HTTPError); ok {
		code = he.Code
	}
	c.Logger().Error(err)
	if code == http.StatusNotFound {
		out, err := errorTemplate.Execute(gonja.Context{"recent": dataLayer.Recent(), "detail": "I think you should try one of the existing pages"})
		if err != nil {
			c.HTML(http.StatusInternalServerError, err.Error())
		} else {
			if err := c.HTML(http.StatusOK, out); err != nil {
				c.Logger().Error(err)
			}
		}
	} else {
		c.HTML(http.StatusInternalServerError, err.Error())
	}
}
