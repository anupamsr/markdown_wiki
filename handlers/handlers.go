package handlers

import (
	"codeberg.org/anupamsr/markdown_wiki/dataLayer"
	"github.com/labstack/echo/v4"
)

// GetHandler implements factory pattern to return closures which share database connection (https://drstearns.github.io/tutorials/gohandlerctx)
func GetHandler(handlerType string, db *dataLayer.DB) echo.HandlerFunc {
	if handlerType == "root" {
		return rootHandler()
	}
	if handlerType == "title" {
		return titleHandler(db)
	}
	panic("Wrong handler type passed")
}
