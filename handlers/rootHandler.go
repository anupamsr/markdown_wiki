package handlers

import (
	"net/http"

	"codeberg.org/anupamsr/markdown_wiki/dataLayer"
	"github.com/labstack/echo/v4"
	"github.com/noirbizarre/gonja"
)

// Pre-compiling the templates at application startup using the
// little Must()-helper function (Must() will panic if FromFile()
// or FromString() will return with an error - that's it).
// It's faster to pre-compile it anywhere at startup and only
// execute the template later.
var mainTemplate = gonja.Must(gonja.FromFile("templates/main.html"))

func rootHandler() func(c echo.Context) error {
	return func(c echo.Context) error {
		// Execute the template per HTTP request
		out, err := mainTemplate.Execute(gonja.Context{"recent": dataLayer.Recent()})
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		return c.HTML(http.StatusOK, out)
	}
}
