from datetime import datetime
import logging

import couchdb
from couchdb.mapping import (
    DateTimeField,
    Document,
    IntegerField,
    TextField,
    ListField,
    DictField,
    Mapping,
)


class Entry(Document):
    """
    id is initialized with title, but is separated so that we can change the title while keeping the id.
    created and modified should not be set. They should be set while writing to the database.
    """

    title = TextField()
    email = TextField()
    name = TextField()
    phone = IntegerField()
    text = TextField()
    created = DateTimeField()
    modified = DateTimeField()


class RecentConfigRecord(Document):
    id = "recent"
    recent_items = ListField(
        DictField(Mapping.build(title=TextField(), time=DateTimeField()))
    )


class DBIdEmptyError(Exception):
    pass


class DBTableNotFoundError(Exception):
    pass


class DBIdNotFoundError(Exception):
    pass


class DB:
    def __init__(
        self, password, server="localhost", port="5984", user="admin", db_name="entries"
    ):
        self.logger = logging.getLogger(__name__)
        self.password = password
        self.user = user
        self.server = server
        self.port = port
        if self.server != "localhost":
            self.logger.error(f"Connecting to {self.server} over insecure HTTP")
        couch_server = couchdb.Server(
            f"http://{self.user}:{self.password}@{self.server}:{self.port}/"
        )
        if db_name not in couch_server:
            raise DBTableNotFoundError(f"{db_name} database not found")
        self.db = couch_server[db_name]

    def changes(self) -> list[TextField]:
        d = self.db.changes(
            descending=True,
            include_docs=False,
            # filter="changes/not_deleted"
        )
        return [
            x["id"]
            for x in d["results"]
            if ("deleted" not in x or not x["deleted"]) and not x["id"].startswith("_")
        ]

    def read_entry(self, doc_id: str) -> Entry:
        if doc_id is None:
            raise DBIdEmptyError("No id provided for target entry")
        if not self.is_present(doc_id):
            raise DBIdNotFoundError(f"{doc_id} not found")
        return Entry.load(self.db, doc_id)

    def write_entry(self, entry: Entry) -> Entry:
        if entry.id is None:
            entry.id = entry.title

        if entry.id in self.db:
            logging.debug(f"Updating {self.db.name}:{entry.id}/{entry.title}")
            orig_entry = entry
            entry = Entry.load(self.db, entry.id)
            entry.title = orig_entry.title or entry.title
            entry.email = orig_entry.email or entry.email
            entry.name = orig_entry.name or entry.name
            entry.phone = orig_entry.phone or entry.phone
            entry.text = orig_entry.text or entry.text
            entry.modified = datetime.now()
        else:
            logging.debug(f"Creating {entry.id}/{entry.title}")
            entry.created = datetime.now()
            entry.modified = None

        entry.store(self.db)
        return entry

    def read_raw(self, doc_id: str) -> Document:
        if doc_id is None:
            raise DBIdEmptyError("No id provided for target entry")
        if not self.is_present(doc_id):
            raise DBIdNotFoundError(f"{doc_id} not found")
        return self.db[doc_id]

    def read_recent_conf(self) -> RecentConfigRecord:
        try:
            return RecentConfigRecord.load(self.db, RecentConfigRecord.id)
        except DBIdNotFoundError:
            self.logger.exception(f"RECENT CONF '{RecentConfigRecord.id}' not found")
            try:
                record = RecentConfigRecord()
                self.write_recent_record(record)
                return record
            except Exception:
                self.logger.exception("RECENT CONF write error")

    def write_recent_record(self, record: RecentConfigRecord) -> RecentConfigRecord:
        logging.debug(f"RECENT CONF Updating {self.db.name}:{record.recent_items}")
        record.store(self.db)
        return record

    def write_raw(self, d: dict):
        self.db.save(d)

    def is_present(self, doc_id: str) -> bool:
        return doc_id in self.db
