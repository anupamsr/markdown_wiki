# markdown_wiki

[[_TOC_]]

Git clone from `https://codeberg.org/anupamsr/markdown_wiki`

## Install
The two components right now are a python/FastAPI based REST server and CouchDB as database server. You will need to install both to run this.

### Database
markdown_wiki uses [Apache CouchDB](https://couchdb.apache.org) in the backend. You will need to create a database (default: **entries**) before server starts working. You can do it via cURL:

    curl -X PUT http://login:password@127.0.0.1:5984/entries

... assuming couchdb is running on localhost on port 5984.

### REST server
Use pip to install the required packages before starting the server

    $ pip install -r requirements.txt
    $ uvicorn main:app --reload --port 8080

That is it.

### Reverse proxy
It is a good idea to run expose this server to the wider world of _Internet_(TM) behind a reverse proxy. Sample config for _Nginx_ as reverse proxy is given below:

    server {
        server_name beta.dont.tech;

        location /static {
            alias /opt/markdown_wiki/static;
            access_log off;
            expires max;
        }

        location / {
            proxy_pass         http://127.0.0.1:8080;
            proxy_read_timeout 60s;
        }

        location ~ /\.ht {
            deny all;
        }
    }

## Generating js
We use browserify.js to generate bundle files that are then sourced in HTML files. For example,

    browserify preview.js -o static/js/preview.js
    browserify show.js -o static/js/show.js

## TODO
In no particular order:
1. UI: The inactive/active thing doesn't work properly with _name_ field. **Difficult**
2. UI: Add field for phone number, also with validation?
4. UI: The edit button should probably be on top and smaller, like wikipedia? Or maybe on the right side, or on the banner?
   1. Add Edit button on top also
5. UI: Add an option to hide the preview pane
6. Add the most important entries on main landing page
7. UI: Add a "Create new entry" button **High priority**
8. ~~Process the internal urls so that they are highlighted differently if they are linking to other entries on the same website. **High priority, Difficult**~~
   1. ~~Differentiate between internal and external urls.~~
   2. ~~Check which url points to existing entry.~~
9.  ~~Remove None from email **High priority**~~
10. UI: Change css to tailwind or something else so that it can be changed _-or-_ change the styling for better UI
11. UI: A horizontal gray line or some other way to differentiate between article and right pane
12. ~~Add the recently edited pages info somewhere. May be a timeseries db for storing events metadata along with couchdb for fast retrieval?~~
13. Minimize calls to server **Difficult, Low priority**
14. Add a login page **Difficult**